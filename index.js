const got = require('got');
const url = require('url');
const Path = require('path');
const xml2js = require('xml2js');

function parseXmlString(string, cb) {
    xml2js.parseString(string, {normalizeTags: true, tagNameProcessors: [xml2js.processors.stripPrefix]}, cb);
}

function generateAuthorizationHeader(username, password) {
    const credentialPair = Buffer.from(`${username}:${password}`, 'utf-8');
    return `Basic ${credentialPair.toString('base64')}`
}

const Webdav = function (uri, username, password) {
    this.uri = url.parse(uri);
    this.username = username;
    this.password = password;
    this.filedescriptors = {};
    this.statCache = {};
};

Webdav.preparePath = function (path) {
    const pathParts = path.split(/[\\/]/);

    for (let i = 0; i < pathParts.length; i++) {
        pathParts[i] = encodeURIComponent(pathParts[i]);
    }

    return pathParts.join('/');
}

Webdav.prototype.access = async function (path, mode, callback) {
    if (typeof (callback) === 'undefined' && typeof (mode) === 'function') {
        callback = mode;
    }

    try {
        const res = await got({
            url: Path.posix.join(this.uri.href, Webdav.preparePath(path)),
            method: 'PROPFIND',
            headers: {'Depth': 0, 'Authorization': generateAuthorizationHeader(this.username, this.password)}
        });

        callback(null);
    } catch (err) {
        callback(err);
    }
};

Webdav.prototype.appendFile = async function (file, data, options, callback) {
    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    try {
        const res = await got({
            url: Path.posix.join(this.uri.href, Webdav.preparePath(file)),
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/x-sabredav-partialupdate',
                'X-Update-Range': 'append',
                'Authorization': generateAuthorizationHeader(this.username, this.password)
            },
            body: data
        });

        callback(null);
    } catch (err) {
        callback(err);
    }
};

Webdav.prototype.chmod = function (path, mode, callback) {
    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

Webdav.prototype.chown = function (path, uid, gid, callback) {
    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

Webdav.prototype.close = function (fd, callback) {
    if (this.filedescriptors.hasOwnProperty(fd) === false) {
        callback('fd isn\'t a valid open file descriptor.');
        return;
    }

    delete this.filedescriptors[fd];
    callback(null);
};

Webdav.prototype.createReadStream = function (path, options) {
    options = options || {};
    options.start = options.start || 0;
    options.end = options.end || '';

    return got.stream({
        url: Webdav.preparePath(path),
        prefixUrl: this.uri.href,
        headers: {
            'Range': 'bytes=' + options.start + '-' + options.end,
            'Authorization': generateAuthorizationHeader(this.username, this.password)
        }
    });
};

Webdav.prototype.createWriteStream = function (path) {
    return got.stream.put({
        url: Webdav.preparePath(path),
        prefixUrl: this.uri.href,
        headers: {'Authorization': generateAuthorizationHeader(this.username, this.password)}
    });
};

Webdav.prototype.lchmod = function (path, mode, callback) {
    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

Webdav.prototype.lchown = function (path, uid, gid, callback) {
    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

Webdav.prototype.link = function (existingPath, newPath, callback) {
    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

Webdav.prototype.lstat = function (path, callback) {
    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

Webdav.prototype.mkdir = async function (path, options, callback) {
    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    const pathSegments = !!options.recursive ? path.split(/[\\/]/) : [path];

    let folderPath = '';
    for (let i = 0; i < pathSegments.length; i++) {
        try {
            folderPath = Path.posix.join(folderPath, pathSegments[i]);

            await got({
                url: Path.posix.join(this.uri.href, Webdav.preparePath(folderPath)),
                method: 'MKCOL',
                headers: {'Authorization': generateAuthorizationHeader(this.username, this.password)}
            });
        } catch (err) {
            if (!options.recursive) {
                // Calling fs.mkdir() when path is a directory that exists results in an error only when recursive is false.
                callback(err);
            }
        }
    }

    callback(null);
};

Webdav.prototype.mkdtemp = function (prefix, options, callback) {
    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

Webdav.prototype.open = function (path, flags, mode, callback) {
    if (typeof (callback) === 'undefined' && typeof (mode) === 'function') {
        callback = mode;
    }

    let highestfd = 0;
    for (const fd in this.filedescriptors) {
        if (fd > highestfd) {
            highestfd = fd;
        }
    }

    this.filedescriptors[highestfd + 1] = path;
    callback(null, highestfd + 1);
};

Webdav.prototype.read = async function (fd, buffer, offset, length, position, callback) {
    if (this.filedescriptors.hasOwnProperty(fd) === false) {
        callback('fd isn\'t a valid open file descriptor.', null);
        return;
    }

    if (offset !== null) {
        throw new Error('Setting offset to anything else than null is currently not supported!');
    }

    try {
        const res = await got({
            url: Path.posix.join(this.uri.href, Webdav.preparePath(this.filedescriptors[fd])),
            responseType: 'buffer',
            headers: {
                'Range': 'bytes=' + position + '-' + (position + length - 1),
                'Authorization': generateAuthorizationHeader(this.username, this.password)
            }
        });

        callback(null, res.body.length, res.body);
    } catch (err) {
        callback(err, null);
    }
};

Webdav.prototype.readdir = async function (path, options, callback) {
    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    const self = this;

    try {
        const res = await got({
            url: Path.posix.join(this.uri.href, Webdav.preparePath(path)),
            method: 'PROPFIND',
            headers: {'Depth': 1, 'Authorization': generateAuthorizationHeader(this.username, this.password)}
        });

        parseXmlString(res.body, function (err, result) {
            if (!!err) {
                callback(err, null);
                return;
            }

            const files = [];
            for (let i = 0; i < result['multistatus']['response'].length; i++) {
                const item = result['multistatus']['response'][i];
                const href = decodeURIComponent(item['href'][0]);
                const requestedPath = decodeURIComponent(Path.posix.join(self.uri.path, Webdav.preparePath(path)));
                const rawFileName = Path.posix.relative(requestedPath, href);

                if (rawFileName.length === 0) {
                    continue;
                }

                if (rawFileName === path) {
                    continue;
                }

                const fileName = decodeURIComponent(rawFileName);
                files.push(options.encoding === 'buffer' ? new Buffer(fileName) : fileName);
            }

            callback(null, files);
        });
    } catch (err) {
        callback(err, null);
    }
};

Webdav.prototype.readFile = async function (path, options, callback) {
    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    try {
        const res = await got({
            url: Path.posix.join(this.uri.href, Webdav.preparePath(path)),
            headers: {'Authorization': generateAuthorizationHeader(this.username, this.password)}
        });

        callback(null, res.body);
    } catch (err) {
        callback(err);
    }
};

Webdav.prototype.readlink = function (path, options, callback) {
    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

Webdav.prototype.realpath = function (path, options, callback) {
    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

Webdav.prototype.rename = async function (oldPath, newPath, callback) {
    try {
        const res = await got({
            url: Path.posix.join(this.uri.href, Webdav.preparePath(oldPath)),
            method: 'MOVE',
            headers: {
                'Destination': url.resolve(this.uri.href, Webdav.preparePath(newPath)),
                'Authorization': generateAuthorizationHeader(this.username, this.password)
            }
        });

        callback(null);
    } catch (err) {
        callback(err);
    }
};

Webdav.prototype.rmdir = async function (path, options, callback) {
    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    try {
        const res = await got({
            url: Path.posix.join(this.uri.href, Webdav.preparePath(path)),
            method: 'DELETE',
            headers: {
                'Depth': options.recursive ? 'infinity' : 0,
                'Authorization': generateAuthorizationHeader(this.username, this.password)
            }
        });

        callback(null);
    } catch (err) {
        callback(err);
    }
};

Webdav.prototype.stat = async function (path, callback) {
    const self = this;

    if (self.statCache.hasOwnProperty(path)) {
        callback(null, self.statCache[path]);
        return;
    }

    try {
        const res = await got({
            url: Path.posix.join(this.uri.href, Webdav.preparePath(path)),
            method: 'PROPFIND',
            headers: {'Depth': 0, 'Authorization': generateAuthorizationHeader(this.username, this.password)}
        });

        parseXmlString(res.body, function (err, result) {
            if (!!err) {
                callback(err, null);
                return;
            }

            const props = result['multistatus']['response'][0]['propstat'][0]['prop'][0];

            const statData = {
                isFile: function () {
                    return props['resourcetype'][0].length === 0;
                },
                isDirectory: function () {
                    return props['resourcetype'][0].length !== 0;
                },
                isBlockDevice: function () {
                    return false;
                },
                isCharacterDevice: function () {
                    return false;
                },
                isSymbolicLink: function () {
                    return false;
                },
                isFIFO: function () {
                    return false;
                },
                isSocket: function () {
                    return false;
                },
                dev: 0,
                ino: props['fileid'] ? parseInt(props['fileid'][0]) : 0,
                mode: 0,
                nlink: 1,
                uid: 1,
                gid: 1,
                rdev: 0,
                size: props['getcontentlength'] ? parseInt(props['getcontentlength'][0]) : 0,
                blksize: 4096,
                blocks: Math.ceil((props['getcontentlength'] ? parseInt(props['getcontentlength'][0]) : 0) / 512),
                mtime: new Date(props['getlastmodified'][0]),
                atime: new Date(),
                ctime: new Date(),
                birthtime: Date.now(),
                mtimeMs: new Date(props['getlastmodified'][0]).getTime() / 1000,
                atimeMs: new Date().getTime() / 1000,
                ctimeMs: new Date().getTime() / 1000,
                birthtimeMs: new Date().getTime() / 1000
            };

            self.statCache[path] = statData;
            callback(null, statData);
        });
    } catch (err) {
        callback(err, null);
    }
};

Webdav.prototype.symlink = function (target, path, type, callback) {
    if (typeof (callback) === 'undefined' && typeof (type) === 'function') {
        callback = type;
    }

    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

Webdav.prototype.truncate = async function (path, len, callback) {
    try {
        const res = await got({
            url: Path.posix.join(this.uri.href, Webdav.preparePath(path)),
            method: 'PUT',
            headers: {
                'Content-Length': len,
                'Authorization': generateAuthorizationHeader(this.username, this.password)
            },
            body: ''
        });

        callback(null);
    } catch (err) {
        callback(err);
    }
};

Webdav.prototype.unlink = async function (path, callback) {
    try {
        const res = await got({
            url: Path.posix.join(this.uri.href, Webdav.preparePath(path)),
            method: 'DELETE',
            headers: {
                'Depth': 0,
                'Authorization': generateAuthorizationHeader(this.username, this.password)
            }
        });

        callback(null);
    } catch (err) {
        callback(err);
    }
};

Webdav.prototype.unwatchFile = function (filename, listener) {
    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

Webdav.prototype.utimes = function (path, atime, mtime, callback) {
    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

Webdav.prototype.watch = function (filename, options, listener) {
    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

Webdav.prototype.watchFile = function (filename, options, listener) {
    throw new Error('Method \'' + arguments.callee.name + '\' not Implemented');
};

Webdav.prototype.write = async function (fd, string, position, encoding, callback) {
    if (typeof position !== 'number') {
        position = 0;
    }

    try {
        const res = await got({
            url: Path.posix.join(this.uri.href, Webdav.preparePath(this.filedescriptors[fd])),
            encoding: encoding || 'utf8',
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/x-sabredav-partialupdate',
                'X-Update-Range': 'bytes=' + position + '-' + (position + string.length),
                'Authorization': generateAuthorizationHeader(this.username, this.password)
            },
            body: string
        });

        callback(null, string.length, string);
    } catch (err) {
        callback(err, 0, null);
    }
};

Webdav.prototype.writeFile = async function (file, data, options, callback) {
    if (typeof (callback) === 'undefined' && typeof (options) === 'function') {
        callback = options;
    }

    try {
        const res = await got({
            url: Path.posix.join(this.uri.href, Webdav.preparePath(file)),
            method: 'PUT',
            headers: {
                'Content-Length': data.length,
                'Authorization': generateAuthorizationHeader(this.username, this.password)
            },
            body: data
        });

        callback(null);
    } catch (err) {
        callback(err);
    }
};

module.exports = Webdav;