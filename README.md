fs-webdav
===================
A drop-in replacement for the `fs` module, for webdav.
Used to treat a webdav location as a filesystem, without having to rewrite your code to switch from `fs` to `fs-webdav`

Setup
-------------
Setting up the webdav filesystem only requires 1 step after which it can be used:

`const FSWebdav = require('fs-webdav');
const fs = new FSWebdav(uri, username, password);`

Using this module
-------------
Simply use all methods the same way as you would with the regular `fs` module

Supported methods:

| Method   | Supported? |
|----------|:-------------:|
| access | YES |
| appendFile | YES |
| chmod | no |
| chown | no |
| close | YES |
| createReadStream | YES |
| createWriteStream | YES |
| lchmod | no |
| lchown | no |
| link | no |
| lstat | no |
| mkdir | YES |
| mkdtemp | no |
| open | YES |
| read | YES |
| readdir | YES |
| readFile | YES |
| readlink | no |
| realpath | no |
| rename | YES |
| rmdir | YES |
| stat | YES |
| symlink | no |
| truncate | YES |
| unlink | YES |
| unwatchFile | no |
| utimes | no |
| watch | no |
| watchFile | no |
| write | YES |
| writeFile | YES |

It could be that some methods with optional arguments aren't implemented the way you expect it to.
If you encounter such method, please file an issue on the Bitbucket and it will be implemented asap. PR's are very welcome too.